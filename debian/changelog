r-cran-viridis (0.6.5-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 01 Feb 2024 17:40:43 -0600

r-cran-viridis (0.6.4-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Switch to virtual debhelper-compat (= 13)

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 22 Jul 2023 18:07:52 -0500

r-cran-viridis (0.6.3-2) unstable; urgency=medium

  * Rebuilding for unstable following bookworm release
  
  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 25 Jun 2023 09:30:43 -0500

r-cran-viridis (0.6.3-1) experimental; urgency=medium

  * New upstream release (into 'experimental' while Debian is frozen)

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Switch to virtual debhelper-compat (= 12)

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 08 May 2023 17:41:48 -0500

r-cran-viridis (0.6.2-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Standards-Version: to current version  

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 13 Oct 2021 17:18:54 -0500

r-cran-viridis (0.6.1-2) unstable; urgency=medium

  * Simple rebuild for unstable following Debian 11 release

  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 16 Aug 2021 17:24:29 -0500

r-cran-viridis (0.6.1-1) experimental; urgency=medium

  * New upstream release (to experimental during freeze)

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 17 May 2021 17:45:05 -0500

r-cran-viridis (0.6.0-1) experimental; urgency=medium

  * New upstream release (to experimental during freeze)

  * debian/control: Set Build-Depends: to recent R version
  * debian/control: Set Standards-Version: to current version  
  * debian/control: Switch to virtual debhelper-compat (= 11)
  * debian/compat: Removed

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 25 Apr 2021 08:53:07 -0500

r-cran-viridis (0.5.1-3) unstable; urgency=medium

  * Rebuilt for r-4.0 transition

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 19 May 2020 20:21:41 -0500

r-cran-viridis (0.5.1-2) unstable; urgency=medium

  * Rebuilding for R 3.5.0 transition
  
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Build-Depends: to 'debhelper (>= 10)'
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Add Vcs-Browser: and Vcs-Git:

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 06 Jun 2018 07:02:10 -0500

r-cran-viridis (0.5.1-1) unstable; urgency=medium

  * New upstream release
  
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Switch from cdbs to dh-r
  * debian/rules: Idem

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 30 Mar 2018 07:48:06 -0500

r-cran-viridis (0.5.0-1) unstable; urgency=medium

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 07 Feb 2018 21:00:40 -0600

r-cran-viridis (0.4.1-1) unstable; urgency=medium

  * New upstream release
  
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Set Build-Depends: to current R version 
  * debian/compat: Set level to 9

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 08 Jan 2018 21:37:18 -0600

r-cran-viridis (0.4.0-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version

  * debian/control: Add new package r-cran-viridislite to (Build-)Depends

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 04 Apr 2017 09:36:46 -0500

r-cran-viridis (0.3.4-1) unstable; urgency=low

  * Initial Debian release 				(Closes: #843124)

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 04 Nov 2016 13:30:16 -0500
